package com.javaclass;

import java.io.*;

class Account
{
    final String bankName = "SBI";
    String custname;
    public int accno;
    private float balance;
    Account()
    {
        custname="";
        accno=-1;
    }
    Account(String custName)
    {
        this.custname=custName;
    }
    Account(String custName,int accno)
    {
        this(custName);//constructor chaining
    }

    public void read()
    {
        InputStreamReader isr= new InputStreamReader(System.in);
        BufferedReader br=new BufferedReader(isr);
        try
        {
            System.out.println("\nEnter the Customer Name: ");
            custname=br.readLine();
            System.out.println("\nEnter Account No: ");
            accno=Integer.parseInt(br.readLine());
            System.out.println("\nEnter account Balance: ");
            balance=Float.valueOf(br.readLine());
        }
        catch(Exception e)
        {
            System.out.println(e.toString());
        }
    }
    public void display()
    {
        System.out.println("Bank Name= "+bankName);
        System.out.println("Account Name= "+custname);
        System.out.println("Account Number= "+accno);
        System.out.println("Balance Amount= "+balance);
    }
  /*public float getBalance()
  {
    return balance;
  }
  public void setBalance(float bal)
  {
  balance=bal;
}*/
} //end of account class

class LoanAccount extends Account
{
    float intRate;
    int amountSanc;
    public LoanAccount()
    {
        super();
        intRate=(float)0.0;
        amountSanc=0;
    }

    public LoanAccount(String custName,int accno,float intRate,int amount)
    {
        super(custName,accno);
        this.intRate=intRate;
        amountSanc=amount;
    }
    public void read()
    {
        InputStreamReader isr=new InputStreamReader(System.in);
        BufferedReader br=new BufferedReader(isr);
        try
        {
            super.read();
            System.out.println("Enter the Interest Rate: ");
            intRate=Float.valueOf(br.readLine());;
            System.out.println("Enter the maximum amount sanctioned");
            amountSanc=Integer.parseInt(br.readLine());
        }
        catch(Exception e)
        {
            System.out.println(e.toString());
        }
    }

    final float interestCalc(){
        return (intRate * amountSanc)/100;
    }

    public void display()
    {
        super.display();
        //access specifiers
        System.out.println("Interest rate: "+intRate);
        System.out.println("Account No: "+accno);
        System.out.println("Maximum amount sanctioned: "+amountSanc);
    }
} //end of loan account

    final class CarLoan extends LoanAccount{

        String carCompany;
        String carModel;
        int carCost;

        CarLoan(){
            carCompany = "Ford";
            carModel = "Mustang";
            carCost = 23000000;
        }

        CarLoan(String custName, int accno, float intRate, int amount, String comp,String model, int cost){
            super(custName, accno, intRate, amount);
            carCompany = comp;
            carModel = model;
            carCost = cost;
        }

        public void read(){
            InputStreamReader isr = new InputStreamReader(System.in);
            BufferedReader br = new BufferedReader(isr);
            try{
                super.read();
                System.out.println("Enter the car company name: ");
                carCompany = br.readLine();
                System.out.println("Enter the car model: ");
                carModel = br.readLine();
                System.out.println("Enter the car cost: ");
                carCost =Integer.parseInt(br.readLine());

            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

        public void display(){
            float totalInterest;
            totalInterest = interestCalc();
            super.display();
            System.out.println("Car company is: "+carCompany);
            System.out.println("Car model is: "+carModel);
            System.out.println("Car cost is: "+carCost);
            System.out.println("Total interest is: "+totalInterest);
        }
    } //End of CarLoan


public class Inheritence {
    public static void main(String []args)
    {
//        X obj = new X();
//        //System.out.println(obj.custName);
//        Account a2=new Account();
//        LoanAccount la = new LoanAccount();
//        la.read();
//        a2.read();
//        System.out.println(la.custname);
//        a2.display();
        //Account acc = new Account();
        //acc.display();
        //LoanAccount l1 = new LoanAccount();
        //l1.disp();
       // LoanAccount l2 = new LoanAccount("damn son",20,430f,10000);
         //l2.disp();
        //CarLoan ca = new CarLoan("Damn nigg u trippin", 1202, (float)12331.2, 10000,"Dammnnn soon","2500", 200000);
        //ca.disp();
        //Account ac = new LoanAccount();
        //ac.display();
        Account ac1 = new CarLoan();
        ac1.display();

    }
}
