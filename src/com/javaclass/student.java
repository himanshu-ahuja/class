package com.javaclass;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class student {

    private int stu_Roll_no;
    private String stu_name;
    private String stu_course;
    private float[] stu_marks;

    private float calculate(){
        float perc,result=0;
        for(byte i = 0;i<5;i++){
            result += stu_marks[i];
        }
        perc = (result*100)/500;
        return perc;
    }

    public void read() throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        stu_marks = new float[5];
        System.out.println("Enter Student Roll Number: ");
        stu_Roll_no = Integer.parseInt(br.readLine());
        System.out.println("Enter Student Name: ");
        stu_name = br.readLine();
        System.out.println("Enter Student Enrolled Course Name: ");
        stu_course = br.readLine();
        for(byte i=0;i<5;i++){
            System.out.println("Enter Subject "+i+" marks: ");
            stu_marks[i] = Float.parseFloat(br.readLine());
        }
    }
    public void display(){
        float percentage;
        percentage = calculate();
        System.out.println("----------Student Details----------");
        System.out.println("Student register number: "+stu_Roll_no);
        System.out.println("Student name: "+stu_name);
        System.out.println("Enrolled Course: "+stu_course);
        for(byte i=0;i<5;i++){
            System.out.println("Subject "+i+" marks: "+stu_marks[i]);
        }
        System.out.println("Student percentage is: "+percentage);
    }

  public static void main(String[] args) throws IOException{
      BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
      boolean choice = true;
      byte menu;

      student stu = new student();

      while(choice){
          System.out.println("1. Enter Data");
          System.out.println("2. Display Data");
          System.out.println("3. Exit");
          System.out.println("Enter Your Choice: ");
          menu = Byte.parseByte(br.readLine());

          switch (menu){
              case 1: stu.read();
              break;
              case 2: stu.display();
              break;
              case 3: choice = false;
          }
      }

  }
}
