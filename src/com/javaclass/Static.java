package com.javaclass;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Static {
    private String cust_Name;
    private byte cust_age;
    private long cust_accNo;
    private char cust_gender;
    private float cust_balance;
    private short cust_minBal;
    private long custID;
    static long scustID;

    public Static()
    {
        scustID++;
        custID = scustID;
    }
    /*
    public Static(String a,byte a1)
    {
        System.out.println("hello");
        cust_Name=a;
        cust_age=a1;
        cust_accNo=-1;
        cust_gender='F';
        cust_minBal=0;
        cust_balance=0;
    }*/
    static{
        int a = 10;
        int b = 20;
        int c = a+b;
        scustID = 100;
        System.out.println(c);
        System.out.println(scustID);
    } //Static Block

    public static void show(){
        System.out.println("Last Employee ID: "+scustID);

    }
    public void read() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("--------CUSTOMER DETAILS--------");
        System.out.println("Enter Customer Name: ");
        cust_Name = br.readLine();
        System.out.println("Enter Customer Age: ");
        cust_age = Byte.parseByte(br.readLine());
        System.out.println("Enter Customer Account Number: ");
        cust_accNo = Long.parseLong(br.readLine());
        System.out.println("Enter Minimum Balance Limit: ");
        cust_minBal = Short.parseShort(br.readLine());
        System.out.println("Enter Customer Account Balance: ");
        cust_balance = Float.parseFloat(br.readLine());
        System.out.println("Enter Customer Gender: ");
        cust_gender = (char) br.read();
    }

    public void display(){
        System.out.println("Customer Name is: "+cust_Name);
        System.out.println("Customer ID: "+scustID);
        System.out.println("Customer Age is: "+cust_age);
        System.out.println("Customer Account Number is: "+cust_accNo);
        System.out.println("Minimum Account Balance is: "+cust_minBal);
        System.out.println("Customer Account Balance is: "+cust_balance);
        System.out.println("Customer Gender is: "+cust_gender);
    }

    /*Search Customer*/
    boolean search(String name) {

        if(cust_Name.equals(name)){
            return true;
        }
        else return false;
    }

    /*Search Account Number*/
    boolean search(long accNo){
        if(cust_accNo == accNo){
            return true;
        }
        else return true;
    }

    /*Search Balance Wise*/
    boolean search(float balance, char gender){
        if(cust_balance >= balance && cust_gender == gender){
            return true;
        }
        else return true;
    }
    public static void main(String[] args) throws IOException {

        Static obj = new Static();
        show();

        /*BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        boolean choice = true;
        Byte answer,search_choice;
        boolean result = false;

        //Static ob1=new Static();
        //ob1.display();
       // Static ob2=new Static("aa",(byte)22);
        //ob2.display();
        Static cust_obj[] = new Static [2];

        while(choice){
            System.out.println("1. Enter Data");
            System.out.println("2. Display Data");
            System.out.println("3. Search Data");
            System.out.println("4. Exit");
            System.out.println("Enter Your Choice: ");
            answer = Byte.parseByte(br.readLine());
            switch(answer){
                case 1: for(byte i =0;i<2;i++){
                    cust_obj[i] = new Static();
                    cust_obj[i].read();
                }
                    break;

                case 2: for(byte i=0;i<2;i++){
                    cust_obj[i].display();
                }
                    break;

                case 3:{

                    System.out.println("1. Search by Customer Name");
                    System.out.println("2. Search by Customer Account Number");
                    System.out.println("3. Search by Balance and Gender");
                    System.out.println("4. Enter Your Choice: ");
                    search_choice = Byte.parseByte(br.readLine());
                    switch(search_choice){

                        case 1: {
                            String name;
                            /*System.out.println("Enter customer name you wish to search: ");
                            name = br.readLine();*/
                          /*  for (byte i = 0; i < 2; i++) {
                                result = cust_obj[i].search(args[0]);
                                if (result == true) {
                                    cust_obj[i].display();
                                    break;
                                }
                            }
                            if(result == false){
                                System.out.println("Customer details not found");
                            }
                        }//Case 1 end
                        break;

                        case 2:{
                            long accno;
                            System.out.println("Enter Customer Account Number you wish to search: ");
                            accno = Long.parseLong(br.readLine());
                            for (byte i = 0; i < 2; i++) {
                                result = cust_obj[i].search(accno);
                                if (result == true) {
                                    cust_obj[i].display();
                                    break;
                                }
                            }
                            if(result == false){
                                System.out.println("Customer details not found");
                            }
                        }//Case 2 end
                        break;

                        case 3:{
                            float bal;
                            char gend;
                            System.out.println("Enter Customer Balance: ");
                            bal = Float.parseFloat(br.readLine());
                            System.out.println("Enter Customer Gender: ");
                            gend = (char) br.read();
                            for (byte i = 0; i < 2; i++) {
                                result = cust_obj[i].search(bal,gend);
                                if (result == true) {
                                    cust_obj[i].display();
                                    break;
                                }
                            }
                            if(result == false){
                                System.out.println("Customer details not found");
                            }
                        }//Case 3 end
                        break;

                    }//End of Inner Switch

                }
                case 4: choice = false;
            }//End of Case
        }//End of While*/
    }//End of Main
}//End of Class

