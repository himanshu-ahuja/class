package com.javaclass;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Customer {

    private String cust_Name; private byte cust_age; private long cust_accNo; private char cust_gender; private float cust_balance; private short cust_minBal;

    public Customer()
    {
        System.out.println("hello");
        cust_Name="ss";
        cust_age=0;
        cust_accNo=-1;
        cust_gender='F';
        cust_minBal=0;
        cust_balance=0;
    }
    public Customer(String a,byte a1)
    {
        System.out.println("hello");
        cust_Name=a;
        cust_age=a1;
        cust_accNo=-1;
        cust_gender='F';
        cust_minBal=0;
        cust_balance=0;
    }

    public void read() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("--------CUSTOMER DETAILS--------");
        System.out.println("Enter Customer Name: ");
        cust_Name = br.readLine();
        System.out.println("Enter Customer Age: ");
        cust_age = Byte.parseByte(br.readLine());
        System.out.println("Enter Customer Account Number: ");
        cust_accNo = Long.parseLong(br.readLine());
        System.out.println("Enter Minimum Balance Limit: ");
        cust_minBal = Short.parseShort(br.readLine());
        System.out.println("Enter Customer Account Balance: ");
        cust_balance = Float.parseFloat(br.readLine());
        System.out.println("Enter Customer Gender: ");
        cust_gender = (char) br.read();

    }

        public void display(){

            System.out.println("Customer Name is: "+cust_Name);
            System.out.println("Customer Age is: "+cust_age);
            System.out.println("Customer Account Number is: "+cust_balance);
            System.out.println("Minimum Account Balance is: "+cust_minBal);
            System.out.println("Customer Account Balance is: "+cust_balance);
            System.out.println("Customer Gender is: "+cust_gender);

        }

    public static  void main(String[] args) throws IOException {


        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        boolean choice = true;
        Byte answer;

        Customer ob1=new Customer();
        ob1.display();
        Customer ob2=new Customer("aa",(byte)22);
        ob2.display();
        Customer cust_obj[] = new Customer[2];

        while(choice){
            System.out.println("1. Enter Data");
            System.out.println("2. Display Data");
            System.out.println("3. Exit");
            System.out.println("Enter Your Choice: ");
            answer = Byte.parseByte(br.readLine());
            switch(answer){
                case 1: for(byte i =0;i<2;i++){
                    cust_obj[i] = new Customer();
                    cust_obj[i].read();
                }
                break;

                case 2: for(byte i=0;i<2;i++){
                    cust_obj[i].display();
                }
                break;

                case 3: choice = false;
            }
        }//End of While
    }//End of Main
}//End of Class
